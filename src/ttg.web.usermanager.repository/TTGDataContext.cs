﻿using System;
using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;

namespace TTG.Web.UserManager.Repository
{
	public class TTGDataContext : DbContext
	{
        public TTGDataContext(DbContextOptions<TTGDataContext> options) : base(options) { }

		public DbSet<User> Users { get; set; }
		public DbSet<Vendor> Vendors { get; set; }
		public DbSet<Business> Businesses { get; set; }
		public DbSet<Branch> Branches { get; set; }

	}
}

