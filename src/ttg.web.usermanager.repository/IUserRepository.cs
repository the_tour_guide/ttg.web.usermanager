﻿using TTG.Web.Core.Common.Models;

namespace TTG.Web.UserManager.Repository
{
    public interface IUserRepository
    {
        Task<int> RegisterUserAsync(User user);
        Task<List<User>> GetUsersAsync();
        Task<bool> DeleteUserAsync(int userID);
        Task<int> UpdateUserAsync(User user);
        // Vendor 
        Task<int> RegisterVendorAsync(Vendor vendor);
        Task<List<Vendor>> GetVendorsAsync();
        Task<bool> DeleteVendorAsync(int vendorId);
        Task<int> UpdateVendorAsync(Vendor vendor);
        // Business 
        Task<int> CreateBusinessAsync(Business business);
        Task<List<Business>> GetBusinessesAsync();
        Task<bool> DeleteBusinessAsync(int businessId);
        Task<int> UpdateBusinessAsync(Business business);
        // Branch 
        Task<int> CreateBranchAsync(Branch branch);
        Task<List<Branch>> GetBranchesAsync();
        Task<bool> DeleteBranchAsync(int branchId);
        Task<int> UpdateBranchAsync(Branch branch);
    }
}

