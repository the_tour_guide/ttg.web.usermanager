﻿using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Data.Repository;

namespace TTG.Web.UserManager.Repository
{
    public class UserRepository : RepoFunctions<User>, IUserRepository
    {
        private readonly TTGDbContext _context;
        public UserRepository(TTGDbContext context)
        {
            _context = context;
        }
        public async Task<int> RegisterUserAsync(User user)
        {
            try
            {
                _context.Add(user);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when creating the user", ex);
            }
        }

        public async Task<List<User>> GetUsersAsync()
        {
            try
            {         
                var users = _context.Users.ToList<User>();
                return users;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when fetching all user", ex);
            }
        }

        public async Task<int> UpdateUserAsync(User user)
        {
            try
            {
                _context.Update(user);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating the user", ex);
            }
        }

        public async Task<bool> DeleteUserAsync(int userID)
        {
            try
            {
                _context.Users.Remove(new User { UserId = userID });
                var result = await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when deleting user", ex);
            }
        }

        public async Task<int> RegisterVendorAsync(Vendor vendor)
        {
            try
            {
                _context.Add(vendor);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when creating the vendor", ex);
            }
        }

        public async Task<List<Vendor>> GetVendorsAsync()
        {
            try
            {
                var vendors = _context.Vendors.ToList<Vendor>();
                return vendors;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when fetching all vendor", ex);
            }
        }

        public async Task<int> UpdateVendorAsync(Vendor vendor)
        {
            try
            {
                _context.Update(vendor);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating the vendor", ex);
            }
        }

        public async Task<bool> DeleteVendorAsync(int vendorId)
        {
            try
            {
                _context.Vendors.Remove(new Vendor { VendorId = vendorId });
                var result = await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when deleting vendor", ex);
            }
        }

        //Business
        public async Task<int> CreateBusinessAsync(Business business)
        {
            try
            {
                _context.Add(business);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when creating the business", ex);
            }
        }

        public async Task<List<Business>> GetBusinessesAsync()
        {
            try
            {
                var businesses = _context.Businesses.ToList<Business>();
                return businesses;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when fetching all business", ex);
            }
        }

        public async Task<int> UpdateBusinessAsync(Business business)
        {
            try
            {
                _context.Update(business);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating the business", ex);
            }
        }

        public async Task<bool> DeleteBusinessAsync(int businessId)
        {
            try
            {
                _context.Businesses.Remove(new Business { BusinessId = businessId });
                var result = await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when deleting business", ex);
            }
        }
        //Branch
        public async Task<int> CreateBranchAsync(Branch branch)
        {
            try
            {
                _context.Add(branch);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when creating the branch", ex);
            }
        }

        public async Task<List<Branch>> GetBranchesAsync()
        {
            try
            {
                var branches = _context.Branches.ToList<Branch>();
                return branches;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when fetching all branch", ex);
            }
        }

        public async Task<int> UpdateBranchAsync(Branch branch)
        {
            try
            {
                _context.Update(branch);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating the branch", ex);
            }
        }

        public async Task<bool> DeleteBranchAsync(int branchId)
        {
            try
            {
                _context.Branches.Remove(new Branch { BranchId = branchId });
                var result = await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when deleting branch", ex);
            }
        }
    }

}

