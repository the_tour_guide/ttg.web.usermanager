﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.UserManager.Businesss;

namespace ttg.web.usermanager.api.Controllers
{
    [Route("UserManager/[controller]")]
    [ApiController]
    public class GetController : ControllerBase
    {
        private readonly IUserManager _userManager;
        public GetController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        [Route("AllUsers")]
        public async Task<ActionResult<Response<User>>> AllUsers()
        {
            return Ok(await _userManager.GetUsersAsync());
        }

        [HttpGet]
        [Route("AllVendors")]
        public async Task<ActionResult<Response<Vendor>>> AllVendors()
        {
            return Ok(await _userManager.GetVendorsAsync());
        }

        [HttpGet]
        [Route("Businesses")]
        public async Task<ActionResult<Response<Business>>> Businesses()
        {
            return Ok(await _userManager.GetVendorsAsync());
        }

        [HttpGet]
        [Route("AllBusinesses")]
        public async Task<ActionResult<Response<Business>>> AllBusinesses()
        {
            return Ok(await _userManager.GetVendorsAsync());
        }

        [HttpGet]
        [Route("Branches")]
        public async Task<ActionResult<Response<Branch>>> Branches()
        {
            return Ok(await _userManager.GetVendorsAsync());
        }

        [HttpGet]
        [Route("AllBranches")]
        public async Task<ActionResult<Response<Branch>>> AllBranches()
        {
            return Ok(await _userManager.GetVendorsAsync());
        }
    }
}