﻿using Microsoft.AspNetCore.Mvc;
using TTG.Web.UserManager.Businesss;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.Core.Common.Models;

namespace TTG.Web.UserManager.Api.Controllers
{
    [Route("UserManager/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IUserManager _userManager;
        public RegisterController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpPost]
        [Route("CreateUser")]
        public async Task<ActionResult<Response<bool>>> CreateUser(Request<User> input)
        {
            return Ok(await _userManager.RegisterUser(input.Data));
        }

        [HttpPost]
        [Route("CreateVendor")]
        public async Task<ActionResult<Response<bool>>> CreateVendor(Request<Vendor> input)
        {
            return Ok(await _userManager.RegisterVendor(input.Data));
        }

        [HttpPost]
        [Route("CreateBusiness")]
        public async Task<ActionResult<Response<bool>>> CreateBusiness(Request<Business> input)
        {
            return Ok(await _userManager.CreateBusiness(input.Data));
        }
        [HttpPost]
        [Route("CreateBranch")]
        public async Task<ActionResult<Response<bool>>> CreateBranch(Request<Branch> input)
        {
            return Ok(await _userManager.CreateBranch(input.Data));
        }
    }    
}