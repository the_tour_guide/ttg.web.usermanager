﻿using System;
using Microsoft.AspNetCore.Mvc;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.UserManager.Businesss;

namespace ttg.web.usermanager.api.Controllers
{
    [Route("UserManager/[controller]")]
    [ApiController]
    public class ManageUserController : ControllerBase
    {
        private readonly IUserManager _userManager;
        public ManageUserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpPut]
        [Route("UpdateUser")]
        public async Task<ActionResult<Response<User>>> UpdateUser(Request<User> input)
        {
            return Ok(await _userManager.UpdateUserAsync(input.Data));
        }

        [HttpPut]
        [Route("UpdateVendor")]
        public async Task<ActionResult<Response<Vendor>>> UpdateVendor(Request<Vendor> input)
        {
            return Ok(await _userManager.UpdateVendorAsync(input.Data));
        }

        [HttpPut]
        [Route("UpdateBusiness")]
        public async Task<ActionResult<Response<Business>>> UpdateBusiness(Request<Business> input)
        {
            return Ok(await _userManager.UpdateBusinessAsync(input.Data));
        }

        [HttpPut]
        [Route("UpdateBranch")]
        public async Task<ActionResult<Response<Branch>>> UpdateBranch(Request<Branch> input)
        {
            return Ok(await _userManager.UpdateBranchAsync(input.Data));
        }

        [HttpDelete]
        [Route("DeleteUser")]
        public async Task<ActionResult<Response<User>>> DeleteUser(Request input)
        {
            return Ok(await _userManager.DeleteUserAsync(input.UserID));
        }

        [HttpDelete]
        [Route("DeleteVendor")]
        public async Task<ActionResult<Response<Vendor>>> DeleteVendor(Request input)
        {
            return Ok(await _userManager.DeleteVendorAsync(input.UserID));
        }

        [HttpDelete]
        [Route("DeleteBusiness")]
        public async Task<ActionResult<Response<Business>>> DeleteBusiness(Request input)
        {
            return Ok(await _userManager.DeleteBusinessAsync(input.UserID));
        }

        [HttpDelete]
        [Route("DeleteBranch")]
        public async Task<ActionResult<Response<Branch>>> DeleteBranch(Request input)
        {
            return Ok(await _userManager.DeleteBranchAsync(input.UserID));
        }
    }
}

