﻿using TTG.Web.UserManager.Repository;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.Core.Common.Models;

namespace TTG.Web.UserManager.Businesss
{
    public class UserManager : IUserManager
    {
        private readonly IUserRepository _userRepository;

        public UserManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Response> RegisterUser(User user)
        {
            try
            {
                var result = await _userRepository.RegisterUserAsync(user);
                return new Response<int>
                {
                    IsSuccesful = true
                };
            }
            catch(Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> GetUsersAsync()
        {
            try
            {
                var result = await _userRepository.GetUsersAsync();
                return new Response<List<User>>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }            
        }

        public async Task<Response> UpdateUserAsync(User user)
        {
            try
            {
                var result = await _userRepository.UpdateUserAsync(user);
                return new Response<int>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response<int>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> DeleteUserAsync(int userID)
        {
            try
            {
                var result = await _userRepository.DeleteUserAsync(userID);
                return new Response
                {
                    IsSuccesful = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> RegisterVendor(Vendor vendor)
        {
            try
            {
                var result = await _userRepository.RegisterVendorAsync(vendor);
                return new Response<int>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> GetVendorsAsync()
        {
            try
            {
                var result = await _userRepository.GetVendorsAsync();
                return new Response<List<Vendor>>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> UpdateVendorAsync(Vendor vendor)
        {
            try
            {
                var result = await _userRepository.UpdateVendorAsync(vendor);
                return new Response<int>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response<int>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> DeleteVendorAsync(int vendorId)
        {
            try
            {
                var result = await _userRepository.DeleteVendorAsync(vendorId);
                return new Response
                {
                    IsSuccesful = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        //Business
        public async Task<Response> CreateBusiness(Business business)
        {
            try
            {
                var result = await _userRepository.CreateBusinessAsync(business);
                return new Response<int>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> GetBusinessesAsync()
        {
            try
            {
                var result = await _userRepository.GetBusinessesAsync();
                return new Response<List<Business>>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> UpdateBusinessAsync(Business business)
        {
            try
            {
                var result = await _userRepository.UpdateBusinessAsync(business);
                return new Response<int>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response<int>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> DeleteBusinessAsync(int businessId)
        {
            try
            {
                var result = await _userRepository.DeleteVendorAsync(businessId);
                return new Response
                {
                    IsSuccesful = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }
        //Branch
        public async Task<Response> CreateBranch(Branch branch)
        {
            try
            {
                var result = await _userRepository.CreateBranchAsync(branch);
                return new Response<int>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> GetBranchesAsync()
        {
            try
            {
                var result = await _userRepository.GetBranchesAsync();
                return new Response<List<Branch>>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> UpdateBranchAsync(Branch branch)
        {
            try
            {
                var result = await _userRepository.UpdateBranchAsync(branch);
                return new Response<int>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response<int>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> DeleteBranchAsync(int branchId)
        {
            try
            {
                var result = await _userRepository.DeleteBranchAsync(branchId);
                return new Response
                {
                    IsSuccesful = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }
    }
}