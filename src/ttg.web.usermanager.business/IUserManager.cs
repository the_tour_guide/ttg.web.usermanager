﻿using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.UserManager.Businesss
{
    public interface IUserManager
    {
        Task<Response> RegisterUser(User user);
        Task<Response> GetUsersAsync();
        Task<Response> DeleteUserAsync(int userID);
        Task<Response> UpdateUserAsync(User user);

        Task<Response> RegisterVendor(Vendor vendor);
        Task<Response> GetVendorsAsync();
        Task<Response> DeleteVendorAsync(int vendorId);
        Task<Response> UpdateVendorAsync(Vendor vendor);

        //Business
        Task<Response> CreateBusiness(Business business);
        Task<Response> GetBusinessesAsync();
        Task<Response> DeleteBusinessAsync(int businessId);
        Task<Response> UpdateBusinessAsync(Business business);
        //Branches
        Task<Response> CreateBranch(Branch branch);
        Task<Response> GetBranchesAsync();
        Task<Response> DeleteBranchAsync(int branchId);
        Task<Response> UpdateBranchAsync(Branch branch);
    }
}